<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormulaPackageModel extends Model
{
    protected $table = "formula_package";

    protected $fillable = [
    	"nama_package"
    ];

    public function formula() {
    	return $this->hasMany('App\PackageHasFormula', 'id_package', 'id');
    }
}
