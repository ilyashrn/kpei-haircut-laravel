<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageHasFormula extends Model
{
    protected $table = "package_has_formula";

    protected $fillable = [
    	"id_package", 
    	"id_formula"
    ];

    public function package() {
    	return $this->belongsTo('App\FormulaPackage', 'id_package', 'id');
    }

    public function formula() {
    	return $this->belongsTo('App\Formula', 'id_formula', 'id');
    }
}
