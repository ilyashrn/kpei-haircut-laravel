<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormulaModel extends Model
{
    protected $table = "formula";

    protected $fillable = [
    	"nama_formula"
    ];

    public function packages() {
    	return $this->hasMany('App\PackageHasMany', 'id_formula', 'id');
    }
}
