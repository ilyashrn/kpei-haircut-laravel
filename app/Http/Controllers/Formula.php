<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FormulaPackageModel;
use App\PackageHasFormula;
use App\FormulaModel;

class Formula extends Controller
{
    public function retrieveSubFormula($id_package) {
    	return FormulaPackageModel::where('id', $id_package)->first();
    }

    public function createSubFormula() {
    	
    }
}
