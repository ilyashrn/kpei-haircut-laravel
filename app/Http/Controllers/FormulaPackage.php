<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Formula;
use App\FormulaPackageModel;

class FormulaPackage extends Controller
{
    public function viewFormulaPackage() {
    	return view('package.index');
    }

    public function selectFormulaPackage($id_package) {
    	$formula = new Formula();
    	$sub_formulas = $formula->retrieveSubFormula($id_package);
		
		return view('package.detail', ['package_detail' => $sub_formulas]);    	
    }
}
