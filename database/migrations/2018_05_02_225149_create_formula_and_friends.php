<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormulaAndFriends extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formula_package', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_package');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('formula', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_formula');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('package_has_formula', function (Blueprint $table) {
            $table->integer('id_package')->unsigned();
            $table->integer('id_formula')->unsigned();

            $table->foreign('id_package')->references('id')->on('formula_package');
            $table->foreign('id_formula')->references('id')->on('formula');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
