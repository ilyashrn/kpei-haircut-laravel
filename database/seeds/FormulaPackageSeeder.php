<?php

use Illuminate\Database\Seeder;
use App\FormulaPackage;

class FormulaPackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FormulaPackage::create(["nama_package" => "Paket Formula Haircut Instrumen Saham IPO"]);
        FormulaPackage::create(["nama_package" => "Paket Formula Haircut Instrumen non-Saham"]);
        FormulaPackage::create(["nama_package" => "Paket Formula Haircut Instrumen Saham non-IPO"]);
    }
}
