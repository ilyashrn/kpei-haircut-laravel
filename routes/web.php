<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('formulapackage', 'FormulaPackage@viewFormulaPackage');
Route::get('formulapackage/selectFormulaPackage/{id}', 'FormulaPackage@selectFormulaPackage');

Route::get('haircutCalculator/getVariables', 'HaircutCalculator@getVariables');
Route::get('haircutCalculator/selectVariable/{id?}', 'HaircutCalculator@selectVariable');
