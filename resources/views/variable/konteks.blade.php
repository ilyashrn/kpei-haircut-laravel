@extends('app')
@section('content')
<main class="main">
        <!-- Breadcrumb-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">Home</li>
          <li class="breadcrumb-item">
            <a href="#">Formula dan Variabel</a>
          </li>
          <li class="breadcrumb-item active">Mengelola Konteks Variabel</li>
          <!-- Breadcrumb Menu-->
          <li class="breadcrumb-menu d-md-down-none">
            <div class="btn-group" role="group" aria-label="Button group">
              <a class="btn" href="./">
              <i class="icon-graph"></i>  Dashboard</a>
              <a class="btn" href="#">
              <i class="icon-settings"></i>  Settings</a>
            </div>
          </li>
        </ol>
        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-6">
                <div class="card">
                  <div class="card-header">
                    <strong>Data Variabe dan Periode Tersedia</strong>
                  </div>
                  <div class="card-body">
                    <div class="row">
                      <div class="form-group col-sm-12">
                        <label for="ccyear">Nama Variabel</label>
                        <select class="form-control" id="ccyear" data-placeholder="Pilih Nama Variabel..." name="variabel">
                          <option></option>
                          <option>Jumlah volume transaksi saham</option>
                          <option>Total Hari bursa</option>
                          <option>Jumlah scriptless shared</option>
                          <option>Total hari transaksi per saham</option>
                          <option>Standar deviasi closing price saham</option>
                          <option>Rata-rata closing price saham</option>
                          <option>Total value transaksi saham</option>
                          <option>Total frekuensi transaksi saham</option>
                          <option>Total SID buyer</option>
                          <option>Nilai ekuitas saham</option>
                          <option>EBITDA saham</option>
                          <option>Price harian saham</option>
                        </select>
                      </div>
                    </div>
                    <!--/.row-->
                  </div>
                </div>
                <div class="card">
                  <div class="card-header">
                    <strong>Metode Pengisian Konteks</strong>
                  </div>
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-12">
                        <label>Metode Penarikan data</label>
                        <select class="form-control" data-placeholder="Pilih metode penarikan.." name="metode">
                          <option></option>
                          <option value="1">Tarik data dari Data Warehouse</option>
                          <option value="2">Upload dari Excel</option>
                        </select>
                      </div>
                    </div>
                    <!--/.row-->
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="card" id="variabel-col" style="display: none;">
                  <div class="card-header">
                    <strong>Periode Tersedia utk Variabel <span id="variabel"></span></strong>
                  </div>
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-6 form-group">
                        <label>Awal Periode pengisian</label>
                        <input type="date" class="form-control" name="">
                      </div>
                      <div class="col-sm-6 form-group">
                        <label>Akhir Periode pengisian</label>
                        <input type="date" class="form-control" name="">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-12">
                        <span class="badge badge-success">Januari 2018</span>
                        <span class="badge badge-success">Februari 2018</span>
                        <span class="badge badge-success">Maret 2018</span>
                        <span class="badge badge-success">April 2018</span>
                        <span class="badge badge-danger">Mei 2018</span>
                        <span class="badge badge-danger">Juni 2018</span>
                        <span class="badge badge-danger">Juli 2018</span>
                        <span class="badge badge-danger">Agustus 2018</span>
                        <span class="badge badge-danger">September 2018</span>
                        <span class="badge badge-danger">Oktober 2018</span>
                        <span class="badge badge-danger">Desember 2018</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card" style="display: none;" id="penarikan-col">
                  <div class="card-header">
                    <strong id="penarikan-strong"></strong>
                  </div>
                  <div class="card-body">
                    <div class="row">
                      <div class="form-group col-sm-12" id="penarikan-div"></div>
                    </div>
                    <!--/.row-->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="card">
            <div class="card-body">
              <button class="btn btn-success"><i class="fa fa-save"></i> Tarik data</button>
              <a href="formula-list.html" class="btn btn-danger">Batalkan</a>
            </div>
          </div>
        </div>
        <!--/.col-->
    </main>
@endsection
@section('additional-js')
<script type="text/javascript">
$('select').select2()
$('select[name="variabel"]').on("change", function() {
$("#variabel-col").css('display', 'block');
$(`#variabel`).html($(this).val())
})
$('select[name="metode"]').on("change", function(e) {
$("#penarikan-col").css('display', 'block');
if ($(this).val() == 1) {
$("#penarikan-strong").html('Tarik Data dari Data Warehouse')
$("#penarikan-div").html(`<label for="ccyear">Tarik data dari Data Warehouse</label>
<select class="form-control " id="ccyear" data-placeholder="Pilih sumber data...">
<option></option>
<option>DW BLOOMBERG</option>
<option>DW DWH</option>
</select>`);
$('select').select2()
} else {
$("#penarikan-strong").html('Upload File Excel')
$("#penarikan-div").html(`
<label>Upload dari Excel</label>
<input type="file" class="form-control" name="">`);
}
})
</script>
@endsection