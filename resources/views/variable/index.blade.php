@extends('app')
@section('content')
<main class="main">
    <!-- Breadcrumb-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">Home</li>
      <li class="breadcrumb-item">
        <a href="#">Variabel</a>
      </li>
      <li class="breadcrumb-item active">Mengelola Variabel</li>
      <!-- Breadcrumb Menu-->
      <li class="breadcrumb-menu d-md-down-none">
        <div class="btn-group" role="group" aria-label="Button group">
          <a class="btn" href="./">
          <i class="icon-graph"></i>  Dashboard</a>
          <a class="btn" href="#">
          <i class="icon-settings"></i>  Settings</a>
        </div>
      </li>
    </ol>
    <div class="container-fluid">
      <div class="animated fadeIn">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header">
                <i class="fa fa-align-justify"></i> List Variabel
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-md-6 mb-3">
                    <a href="variable-baru.html" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Variabel Baru</a>
                  </div>
                  <div class="col-md-12">
                    <table class="table table-responsive-sm table-sm">
                      <thead>
                        <tr>
                          <th width="20%">Nama Variabel</th>
                          <th>Penggunaan di Formula</th>
                          <th>Status</th>
                          <th>Waktu Dibuat</th>
                          <th width="15%">Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Jumlah volume transaksi saham</td>
                          <td>
                            <span class="badge badge-success">Percentage Volume Agunan/Scripless Shared</span>
                            <span class="badge badge-success">Percentage Day Trading</span>
                          </td>
                          <td>
                            <span class="badge badge-success">Aktif</span>
                          </td>
                          <td>2012/01/01</td>
                          <td>
                            <a href="#" class="btn btn-sm btn-outline-primary"><i class="fa fa-pencil"></i> Ubah</a>
                            <a href="#" class="btn btn-sm btn-danger" onclick="return confirm('Anda yakin?')"><i class="fa fa-times"></i> Hapus</a>
                          </td>
                        </tr>
                        <tr>
                          <td>Total Hari bursa</td>
                          <td>
                            <span class="badge badge-success">Percentage Day Trading</span>
                            <span class="badge badge-success">Volaitilitas</span>
                          </td>
                          <td>
                            <span class="badge badge-danger">Dihapus</span>
                          </td>
                          <td>2012/01/01</td>
                          <td>
                            <a href="#" class="btn btn-sm btn-outline-primary"><i class="fa fa-pencil"></i> Ubah</a>
                            <a href="#" class="btn btn-sm btn-danger" onclick="return confirm('Anda yakin?')"><i class="fa fa-times"></i> Hapus</a>
                          </td>
                        </tr>
                        <tr>
                          <td>Jumlah scriptless shared</td>
                          <td>
                            <span class="badge badge-success">Percentage Volume Agunan/Scripless Shared</span>
                          </td>
                          <td>
                            <span class="badge badge-success">Aktif</span>
                          </td>
                          <td>2012/01/01</td>
                          <td>
                            <a href="#" class="btn btn-sm btn-outline-primary"><i class="fa fa-pencil"></i> Ubah</a>
                            <a href="#" class="btn btn-sm btn-danger" onclick="return confirm('Anda yakin?')"><i class="fa fa-times"></i> Hapus</a>
                          </td>
                        </tr>
                        <tr>
                          <td>Total hari transaksi per saham</td>
                          <td>
                            <span class="badge badge-success">Volaitilitas</span>
                          </td>
                          <td>
                            <span class="badge badge-success">Aktif</span>
                          </td>
                          <td>2012/01/01</td>
                          <td>
                            <a href="#" class="btn btn-sm btn-outline-primary"><i class="fa fa-pencil"></i> Ubah</a>
                            <a href="#" class="btn btn-sm btn-danger" onclick="return confirm('Anda yakin?')"><i class="fa fa-times"></i> Hapus</a>
                          </td>
                        </tr>
                        <tr>
                          <td>Standar deviasi closing price saham</td>
                          <td>
                            <span class="badge badge-success">Percentage Volume Agunan/Scripless Shared</span>
                            <span class="badge badge-success">Percentage Day Trading</span>
                          </td>
                          <td>
                            <span class="badge badge-danger">Dihapus</span>
                          </td>
                          <td>2012/01/01</td>
                          <td>
                            <a href="#" class="btn btn-sm btn-outline-primary"><i class="fa fa-pencil"></i> Ubah</a>
                            <a href="#" class="btn btn-sm btn-danger" onclick="return confirm('Anda yakin?')"><i class="fa fa-times"></i> Hapus</a>
                          </td>
                        </tr>
                        <tr>
                          <td>Rata-rata closing price saham</td>
                          <td>
                            <span class="badge badge-success">Percentage Volume Agunan/Scripless Shared</span>
                            <span class="badge badge-success">Percentage Day Trading</span>
                            <span class="badge badge-success">Volaitilitas</span>
                            <span class="badge badge-success">Rata-rata SID Buyer</span>
                          </td>
                          <td>
                            <span class="badge badge-success">Aktif</span>
                          </td>
                          <td>2012/01/01</td>
                          <td>
                            <a href="#" class="btn btn-sm btn-outline-primary"><i class="fa fa-pencil"></i> Ubah</a>
                            <a href="#" class="btn btn-sm btn-danger" onclick="return confirm('Anda yakin?')"><i class="fa fa-times"></i> Hapus</a>
                          </td>
                        </tr>
                        <tr>
                          <td>Total value transaksi saham</td>
                          <td>
                            <span class="badge badge-success">Percentage Volume Agunan/Scripless Shared</span>
                            <span class="badge badge-success">Percentage Day Trading</span>
                          </td>
                          <td>
                            <span class="badge badge-success">Aktif</span>
                          </td>
                          <td>2012/01/01</td>
                          <td>
                            <a href="#" class="btn btn-sm btn-outline-primary"><i class="fa fa-pencil"></i> Ubah</a>
                            <a href="#" class="btn btn-sm btn-danger" onclick="return confirm('Anda yakin?')"><i class="fa fa-times"></i> Hapus</a>
                          </td>
                        </tr>
                        <tr>
                          <td>Total frekuensi transaksi saham</td>
                          <td>
                            <span class="badge badge-success">Percentage Volume Agunan/Scripless Shared</span>
                          </td>
                          <td>
                            <span class="badge badge-success">Aktif</span>
                          </td>
                          <td>2012/01/01</td>
                          <td>
                            <a href="#" class="btn btn-sm btn-outline-primary"><i class="fa fa-pencil"></i> Ubah</a>
                            <a href="#" class="btn btn-sm btn-danger" onclick="return confirm('Anda yakin?')"><i class="fa fa-times"></i> Hapus</a>
                          </td>
                        </tr>
                        <tr>
                          <td>Total SID buyer</td>
                          <td>
                            <span class="badge badge-success">Percentage Volume Agunan/Scripless Shared</span>
                            <span class="badge badge-success">Percentage Day Trading</span>
                            <span class="badge badge-success">Volaitilitas</span>
                            <span class="badge badge-success">Rata-rata SID Buyer</span>
                            <span class="badge badge-success">Rata-rata Frekuensi Saham</span>
                            <span class="badge badge-success">AVG Volume Transaksi/Scripless Shared</span>
                          </td>
                          <td>
                            <span class="badge badge-success">Aktif</span>
                          </td>
                          <td>2012/01/01</td>
                          <td>
                            <a href="#" class="btn btn-sm btn-outline-primary"><i class="fa fa-pencil"></i> Ubah</a>
                            <a href="#" class="btn btn-sm btn-danger" onclick="return confirm('Anda yakin?')"><i class="fa fa-times"></i> Hapus</a>
                          </td>
                        </tr>
                        <tr>
                          <td>Nilai ekuitas saham</td>
                          <td>
                            <span class="badge badge-success">Percentage Volume Agunan/Scripless Shared</span>
                            <span class="badge badge-success">Percentage Day Trading</span>
                          </td>
                          <td>
                            <span class="badge badge-success">Aktif</span>
                          </td>
                          <td>2012/01/01</td>
                          <td>
                            <a href="#" class="btn btn-sm btn-outline-primary"><i class="fa fa-pencil"></i> Ubah</a>
                            <a href="#" class="btn btn-sm btn-danger" onclick="return confirm('Anda yakin?')"><i class="fa fa-times"></i> Hapus</a>
                          </td>
                        </tr>
                        <tr>
                          <td>EBITDA saham</td>
                          <td>
                            <span class="badge badge-success">Percentage Volume Agunan/Scripless Shared</span>
                            <span class="badge badge-success">Percentage Day Trading</span>
                          </td>
                          <td>
                            <span class="badge badge-danger">Dihapus</span>
                          </td>
                          <td>2012/01/01</td>
                          <td>
                            <a href="#" class="btn btn-sm btn-outline-primary"><i class="fa fa-pencil"></i> Ubah</a>
                            <a href="#" class="btn btn-sm btn-danger" onclick="return confirm('Anda yakin?')"><i class="fa fa-times"></i> Hapus</a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                    <ul class="pagination">
                      <li class="page-item">
                        <a class="page-link" href="#">Prev</a>
                      </li>
                      <li class="page-item active">
                        <a class="page-link" href="#">1</a>
                      </li>
                      <li class="page-item">
                        <a class="page-link" href="#">2</a>
                      </li>
                      <li class="page-item">
                        <a class="page-link" href="#">3</a>
                      </li>
                      <li class="page-item">
                        <a class="page-link" href="#">4</a>
                      </li>
                      <li class="page-item">
                        <a class="page-link" href="#">Next</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!--/.col-->
        </div>
        <!--/.row-->
      </div>
    </div>
  </main>
@endsection