<!DOCTYPE html>
<!--
* CoreUI - Free Bootstrap Admin Template
* @version v2.0.0-beta.0
* @link https://coreui.io
* Copyright (c) 2018 creativeLabs Łukasz Holeczek
* Licensed under MIT (https://coreui.io/license)
-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <title>Sistem Perhitungan Nilai Haircut Agunan - KPEI</title>
    <!-- Icons-->
    <link href="{{ asset('assets/node_modules/flag-icon-css/css/flag-icon.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/node_modules/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/node_modules/simple-line-icons/css/simple-line-icons.css') }}" rel="stylesheet">
    <!-- Main styles for this application-->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/dist/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/css/pace.min.css') }}" rel="stylesheet">
  </head>
  <body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
    <header class="app-header navbar">
      <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
      <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="#">
        <img class="navbar-brand-full" src="{{ asset('assets/img/site-logo.png') }}" alt="CoreUI Logo" width="50%" height="60%">
        <img class="navbar-brand-minimized" src="{{ asset('assets/img/site-logo.png') }}" width="30" height="30" alt="CoreUI Logo">
      </a>
      <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
      <span class="navbar-toggler-icon"></span>
      </button>
      <ul class="nav navbar-nav ml-auto">
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            <img class="img-avatar" src="{{ asset('assets/img/avatars/6.jpg') }}" alt="admin@bootstrapmaster.com">
          </a>
          <div class="dropdown-menu dropdown-menu-right">
            <div class="dropdown-header text-center">
              <strong>Akun User</strong>
            </div>
            <a class="dropdown-item" href="#">
            <i class="fa fa-user"></i> Profile</a>
            <a class="dropdown-item" href="#">
            <i class="fa fa-wrench"></i> Settings</a>
            <a class="dropdown-item" href="#">
            <i class="fa fa-lock"></i> Logout</a>
          </div>
        </li>
      </ul>
    </header>
    <div class="app-body">
      <div class="sidebar">
        <nav class="sidebar-nav">
          <ul class="nav">
            <li class="nav-item">
              <a class="nav-link" href="index.html">
                <i class="nav-icon icon-speedometer"></i> Dashboard
              </a>
            </li>
            <li class="nav-title">Formula dan Variabel</li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
              <i class="nav-icon icon-puzzle"></i> Konteks/Variabel</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="{{ url('haircutCalculator/getVariables') }}">Mengelola Variabel</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{ url('haircutCalculator/selectVariable') }}">Mengelola Konteks</a>
                </li>
              </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
              <i class="nav-icon icon-puzzle"></i> Formula</a>
              <ul class="nav-dropdown-items open">
                <li class="nav-item active">
                  <a class="nav-link" href="{{ url('formulapackage') }}">Mengelola Formula</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{ url('formulapackage') }}">Tambah Formula</a>
                </li>
              </ul>
            </li>
            <li class="divider"></li>
            <li class="nav-title">Perhitungan Haircut</li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
              <i class="nav-icon icon-star"></i> Instrumen</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="hitung-haircut.html" target="_top">
                  <i class="nav-icon icon-star"></i> Non-Saham</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="hitung-haircut.html" target="_top">
                  <i class="nav-icon icon-star"></i> Saham IPO</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="hitung-haircut.html" target="_top">
                  <i class="nav-icon icon-star"></i> Saham non-IPO</a>
                </li>
              </ul>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="hasil-hitung.html">
              <i class="nav-icon icon-drop"></i> Hasil Perhitungan</a>
            </li>
          </ul>
        </nav>
        <button class="sidebar-minimizer brand-minimizer" type="button"></button>
      </div>
      @yield('content')
    </div>
    <footer class="app-footer">
      <div>
        <a href="https://coreui.io">Sistem Perhitungan Nilai Haircut Agunan @ KPEI</a>
        <span>&copy; 2018 AKPSI Group #2.</span>
      </div>
      <div class="ml-auto">
        <span>Powered by</span>
        <a href="https://coreui.io">CoreUI</a>
      </div>
    </footer>
    <!-- Bootstrap and necessary plugins-->
    <script src="{{ asset('assets/node_modules/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/dist/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/popper.js/dist/umd/popper.min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/pace-progress/pace.min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/@coreui/coreui/dist/js/coreui.min.js') }}"></script>
    @yield('additional-js')
  </body>
</html>