@extends('app')
@section('content')
<main class="main">
	<!-- Breadcrumb-->
	<ol class="breadcrumb">
		<li class="breadcrumb-item">Home</li>
		<li class="breadcrumb-item">
			<a href="#">Formula dan Variabel</a>
		</li>
		<li class="breadcrumb-item active">Ubah Paket Formula</li>
		<!-- Breadcrumb Menu-->
		<li class="breadcrumb-menu d-md-down-none">
			<div class="btn-group" role="group" aria-label="Button group">
				<a class="btn" href="./">
				<i class="icon-graph"></i>  Dashboard</a>
				<a class="btn" href="#">
				<i class="icon-settings"></i>  Settings</a>
			</div>
		</li>
	</ol>
	<div class="container-fluid">
		<div class="animated fadeIn">
			<div class="row">
				<div class="col-sm-12">
					<div class="card">
						<div class="card-header">
							<strong>Data Dasar Paket Formula</strong>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="name">Name Paket Formula</label>
										<input type="text" class="form-control" id="name" placeholder="Nama Formula..." value="{{ $package_detail->nama_package }}">
									</div>
								</div>
								<div class="form-group col-sm-6">
									<label for="ccyear">Status</label>
									<select class="form-control" id="ccyear" data-placeholder="Pilih Paket Formula...">
										<option></option>
										<option selected="">Active</option>
										<option>Disabled</option>
									</select>
								</div>
							</div>
							<!--/.row-->
						</div>
					</div>
				</div>
				<!--/.col-->
				<div class="col-sm-12">
					<div class="row" id="variable-sets">
						<div class="col-sm-6" id="variable-set1">
							<div class="card">
								<div class="card-header">
									<strong>Formula Terlibat</strong>
									<small>#1</small>
									<span style="cursor: pointer; color: red;" onclick="hapusvarset(1)"><i class="fa fa-times"></i></span>
								</div>
								<div class="card-body">
									<div class="form-group">
										<label>Nama Formula</label>
										<div class="row">
											<div class="col-sm-6">
												<select class="form-control" id="ccyear" data-placeholder="Pilih nama formula...">
													<option></option>
													<option>Percentage Volume Agunan/Scripless Shared</option>
													<option>Percentage Day Trading</option>
													<option>Volaitilitas</option>
													<option>Percentage Day Trading</option>
													<option>Rata-rata SID Buyer</option>
													<option>Rata-Rata Frekuensi Saham</option>
													<option>AVG Volume Transaksi/Scripless Shared</option>
													<option>Liquidity Risk Eligibility</option>
													<option>Market Risk Eligibility</option>
													<option>VaR Eligibility</option>
												</select>
											</div>
											<div class="col-sm-6">
												<a href="formula-baru.html"><i class="fa fa-plus"></i> Formula tidak ditemukan? Tambah Formula</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="card">
								<div class="card-body">
									<a href="#" onclick="tambahvarset()"><i class="fa fa-plus"></i> Tambah formula terlibat</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--/.col-->
				<div class="col-sm-12">
					<div class="card">
						<div class="card-body">
							<button class="btn btn-success"><i class="fa fa-save"></i> Simpan Formula Package</button>
							<a href="formula-list.html" class="btn btn-danger">Batalkan</a>
						</div>
					</div>
				</div>
			</div>
			<!--/.row-->
		</div>
	</div>
</main>
@endsection
@section('additional-js')
<script type="text/javascript">
    $('select').select2()
    var var_counter = 1;
    
    function tambahvarset() {
    ++var_counter;
    $('#variable-sets').append(
    `<div class="col-sm-6" id="variable-set`+var_counter+`">
        <div class="card">
          <div class="card-header">
            <strong>Formula Terlibat</strong>
            <small>#`+var_counter+`</small>
            <span style="cursor: pointer; color: red;" onclick="hapusvarset(`+var_counter+`)"><i class="fa fa-times"></i></span>
          </div>
          <div class="card-body">
            <div class="form-group">
              <label>Nama Formula</label>
              <div class="row">
                <div class="col-sm-6">
                    <select class="form-control" id="ccyear" data-placeholder="Pilih nama formula...">
                      <option></option>
                      <option>Percentage Volume Agunan/Scripless Shared</option>
                      <option>Percentage Day Trading</option>
                      <option>Volaitilitas</option>
                      <option>Percentage Day Trading</option>
                      <option>Rata-rata SID Buyer</option>
                      <option>Rata-Rata Frekuensi Saham</option>
                      <option>AVG Volume Transaksi/Scripless Shared</option>
                      <option>Liquidity Risk Eligibility</option>
                      <option>Market Risk Eligibility</option>
                      <option>VaR Eligibility</option>
                    </select>
                  </div>
                  <div class="col-sm-6">
                    <a href="formula-baru.html"><i class="fa fa-plus"></i> Formula tidak ditemukan? Tambah Formula</a>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>`)
    $('select').select2()
    }

    function hapusvarset(index) {
      $('#variable-set'+index).remove();
    }
    </script>
@endsection



