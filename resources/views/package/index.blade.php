@extends('app')
@section('content')
<main class="main">
	<!-- Breadcrumb-->
	<ol class="breadcrumb">
		<li class="breadcrumb-item">Home</li>
		<li class="breadcrumb-item">
			<a href="#">Formula dan Variabel</a>
		</li>
		<li class="breadcrumb-item active">Mengelola Formula</li>
		<!-- Breadcrumb Menu-->
		<li class="breadcrumb-menu d-md-down-none">
			<div class="btn-group" role="group" aria-label="Button group">
				<a class="btn" href="./">
				<i class="icon-graph"></i>  Dashboard</a>
				<a class="btn" href="#">
				<i class="icon-settings"></i>  Settings</a>
			</div>
		</li>
	</ol>
	<div class="container-fluid">
		<div class="animated fadeIn">
			<div class="row">
				<div class="col-lg-12">
					<div class="card">
						<div class="card-header">
							<i class="fa fa-align-justify"></i> List Formula
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-md-6 mb-3">
									<a href="formula-baru.html" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Formula Baru</a>
								</div>
								<div class="col-md-12">
									<table class="table table-responsive-sm table-sm">
										<thead>
											<tr>
												<th>Nama Formula Package</th>
												<th>Nama-nama Formula</th>
												<th>Status</th>
												<th>Waktu Dibuat</th>
												<th width="15%">Aksi</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>
													<a href="{{ url('formulapackage/selectFormulaPackage/1') }}"><span class="badge badge-primary">Paket Formula Haircut Instrumen Saham-IPO</span></a>
												</td>
												<td>
													<span class="badge badge-success">Percentage Volume Agunan/Scripless Shared</span>
													<span class="badge badge-success">Percentage Day Trading</span>
													<span class="badge badge-success">Volaitilitas</span>
													<span class="badge badge-success">Percentage Day Trading</span>
													<span class="badge badge-success">Rata-rata SID Buyer</span>
													<span class="badge badge-success">Rata-Rata Frekuensi Saham</span>
													<span class="badge badge-success">AVG Volume Transaksi/Scripless Shared</span>
												</td>
												<td>
													<span class="badge badge-success">Aktif</span>
												</td>
												<td>2012/01/01</td>
												<td>
													<a href="package-formula.html" class="btn btn-sm btn-outline-primary"><i class="fa fa-pencil"></i> Ubah</a>
													<a href="#" class="btn btn-sm btn-danger" onclick="return confirm('Anda yakin?')"><i class="fa fa-times"></i> Hapus</a>
												</td>
											</tr>
											<tr>
												<td>
													<a href="{{ url('formulapackage/selectFormulaPackage/2') }}"><span class="badge badge-warning">Paket Formula Haircut Instrumen non-Saham</span></a>
												</td>
												<td>
													<span class="badge badge-success">Percentage Volume Agunan/Scripless Shared</span>
													<span class="badge badge-success">Percentage Day Trading</span>
													<span class="badge badge-success">Rata-Rata Frekuensi Saham</span>
													<span class="badge badge-success">AVG Volume Transaksi/Scripless Shared</span>
												</td>
												<td>
													<span class="badge badge-danger">Dihapus</span>
												</td>
												<td>2012/01/01</td>
												<td>
													<a href="package-formula.html" class="btn btn-sm btn-outline-primary"><i class="fa fa-pencil"></i> Ubah</a>
													<a href="#" class="btn btn-sm btn-danger" onclick="return confirm('Anda yakin?')"><i class="fa fa-times"></i> Hapus</a>
												</td>
											</tr>
											<tr>
												<td>
													<a href="{{ url('formulapackage/selectFormulaPackage/3') }}"><span class="badge badge-success">Paket Formula Haircut Instrumen Saham non-IPO</span></a>
												</td>
												<td>
													<span class="badge badge-success">Percentage Volume Agunan/Scripless Shared</span>
													<span class="badge badge-success">Percentage Day Trading</span>
													<span class="badge badge-success">Volaitilitas</span>
													<span class="badge badge-success">Percentage Day Trading</span>
													<span class="badge badge-success">Rata-rata SID Buyer</span>
													<span class="badge badge-success">Rata-Rata Frekuensi Saham</span>
													<span class="badge badge-success">AVG Volume Transaksi/Scripless Shared</span>
													<span class="badge badge-success">Liquidity Risk Eligibility</span>
													<span class="badge badge-success">Market Risk Eligibility</span>
													<span class="badge badge-success">VaR Eligibility</span>
												</td>
												<td>
													<span class="badge badge-success">Aktif</span>
												</td>
												<td>2012/01/01</td>
												<td>
													<a href="package-formula.html" class="btn btn-sm btn-outline-primary"><i class="fa fa-pencil"></i> Ubah</a>
													<a href="#" class="btn btn-sm btn-danger" onclick="return confirm('Anda yakin?')"><i class="fa fa-times"></i> Hapus</a>
												</td>
											</tr>
										</tbody>
									</table>
									<ul class="pagination">
										<li class="page-item">
											<a class="page-link" href="#">Prev</a>
										</li>
										<li class="page-item active">
											<a class="page-link" href="#">1</a>
										</li>
										<li class="page-item">
											<a class="page-link" href="#">2</a>
										</li>
										<li class="page-item">
											<a class="page-link" href="#">3</a>
										</li>
										<li class="page-item">
											<a class="page-link" href="#">4</a>
										</li>
										<li class="page-item">
											<a class="page-link" href="#">Next</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--/.col-->
			</div>
			<!--/.row-->
		</div>
	</div>
</main>
@endsection